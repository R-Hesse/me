<?php

$errors = [];
$name = '';
$email = '';
$message = '';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
  // Validate name
  if (empty($_POST['name'])) {
    $errors['name'] = 'Name is required';
  } else {
    $name = $_POST['name'];
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $errors['name'] = 'Only letters and white space allowed';
    }
  }

  // Validate email
  if (empty($_POST['email'])) {
    $errors['email'] = 'Email is required';
  } else {
    $email = $_POST['email'];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $errors['email'] = 'Invalid email format';
    }
  }

  // Validate message
  if (empty($_POST['message'])) {
    $errors['message'] = 'Message is required';
  } else {
    $message = $_POST['message'];
  }

  // If there are no errors, process the form data
  if (empty($errors)) {
    // Process the form data here
    // For example, send an email to the website owner with the user's message
    $to = 'rhesse.career@gmail.com';
    $subject = 'New message from your website';
    $body = "Name: $name\n\nEmail: $email\n\nMessage:\n$message";
    $headers = 'From: webmaster@example.com' . "\r\n" .
      'Reply-To: ' . $email . "\r\n" .
      'X-Mailer: PHP/' . phpversion();

    mail($to, $subject, $body, $headers);

    // Redirect the user to a thank you page
    header('Location: thank-you.html');
    exit;
  }
}

?>
