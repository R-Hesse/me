document.addEventListener('DOMContentLoaded', () => {
    const questions = [
        {
            question: "What is the default port for MySQL?",
            answers: ["3306", "8080", "1433", "1521"],
            correct: 0
        },
        {
            question: "Which command is used to create a new Git branch?",
            answers: ["git branch", "git checkout", "git merge", "git branch new"],
            correct: 1
        },
        {
            question: "Which Azure service is used for automating deployment pipelines?",
            answers: ["Azure Functions", "Azure DevOps", "Azure Kubernetes Service", "Azure Logic Apps"],
            correct: 1
        },
        {
            question: "In Python, how do you create a virtual environment?",
            answers: ["venv init", "python -m venv", "virtualenv", "mkvirtualenv"],
            correct: 1
        },
        {
            question: "Which Java keyword is used to inherit a class?",
            answers: ["super", "extends", "implements", "this"],
            correct: 1
        },
        {
            question: "What does IaC stand for in cloud computing?",
            answers: ["Internet as Code", "Infrastructure as a Computer", "Infrastructure as Code", "Integration as Code"],
            correct: 2
        },
        {
            question: "Which command is used to apply configurations in Terraform?",
            answers: ["terraform apply", "terraform config", "terraform plan", "terraform build"],
            correct: 0
        },
        {
            question: "What is the primary purpose of a star schema in a database?",
            answers: ["Data normalization", "Transaction processing", "Data denormalization for analysis", "Password storage"],
            correct: 2
        },
        {
            question: "Which Python library is widely used for data analysis?",
            answers: ["Django", "Flask", "NumPy", "Pandas"],
            correct: 3
        },
        {
            question: "In Azure DevOps, what file is commonly used to define CI/CD pipelines?",
            answers: ["azure-pipeline.json", "azure-pipeline.yml", "build-pipeline.yml", "ci-cd-pipeline.yml"],
            correct: 1
        },
        {
            question: "Which of the following is NOT a principle of DevOps?",
            answers: ["Continuous Integration", "Continuous Deployment", "Minimal Documentation", "Infrastructure as Code"],
            correct: 2
        },
        {
            question: "How do you horizontally scale a database in Azure?",
            answers: ["Increase compute units", "Add read replicas", "Vertical scaling", "Increase storage size"],
            correct: 1
        },
        {
            question: "What is the purpose of Docker in development?",
            answers: ["Version control", "Web hosting", "Containerization", "Project management"],
            correct: 2
        }
    ];

    let currentQuestionIndex = 0;
    let score = 0;
    let selectedAnswerIndex = -1;

    const questionElement = document.getElementById('question');
    const answersElement = document.getElementById('answers');
    const submitButton = document.getElementById('submit');
    const scoreElement = document.getElementById('score');
    const progressBar = document.getElementById('progress-bar');
    const correctSound = document.getElementById('correct-answer-sound');
    const incorrectSound = document.getElementById('incorrect-answer-sound');

    function playSound(isCorrect) {
        if (isCorrect) {
            correctSound.play();
        } else {
            incorrectSound.play();
        }
    }

    function displayQuestion() {
        const currentQuestion = questions[currentQuestionIndex];
        questionElement.textContent = currentQuestion.question;
        answersElement.innerHTML = '';

        currentQuestion.answers.forEach((answer, index) => {
            const answerLi = document.createElement('li');
            answerLi.textContent = answer;
            answerLi.addEventListener('click', function() {
                selectedAnswerIndex = index;
                Array.from(answersElement.children).forEach(child => child.classList.remove('selected'));
                this.classList.add('selected');
                submitButton.disabled = false;
            });
            answersElement.appendChild(answerLi);
        });
    }

    function updateProgressBar() {
        const progressPercentage = (currentQuestionIndex / questions.length) * 100;
        progressBar.style.width = `${progressPercentage}%`;
    }

    submitButton.addEventListener('click', () => {
        const isCorrect = selectedAnswerIndex === questions[currentQuestionIndex].correct;
        playSound(isCorrect);

        if (isCorrect) {
            score++;
        }

        currentQuestionIndex++;
        if (currentQuestionIndex < questions.length) {
            displayQuestion();
        } else {
            displayScore();
        }
        updateProgressBar(); // Moved to ensure progress bar updates appropriately
        submitButton.disabled = true; 
        selectedAnswerIndex = -1;
    });

    function displayScore() {
        questionElement.textContent = "Game Over!";
        answersElement.innerHTML = '';
        scoreElement.textContent = `Your score: ${score}/${questions.length}`;
        submitButton.style.display = 'none';
    }

    displayQuestion();
});
